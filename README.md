&emsp;



> &#x1F34E; 连享会 - 生存分析专题 (Survival Analysis)    
> &#x1F34F; 课程大纲 PDF 版：[V1](https://gitee.com/arlionn/st/raw/master/PDF_%E8%BF%9E%E4%BA%AB%E4%BC%9A-%E7%8E%8B%E5%AD%98%E5%90%8C%E6%95%99%E6%8E%88-%E7%94%9F%E5%AD%98%E5%88%86%E6%9E%90%E4%B8%93%E9%A2%98-2020.6.6.pdf) &emsp; [V2](https://quqi.gblhgk.com/s/880197/DmHRC3s9U1x5ldTo) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/背景-苔藓001.jpg)

---



**目录**

[TOC]

---



> - 一个网红能红多久？
> - 婚姻难逃「七年之痒」吗？
> - 「老字号」企业能撑多久？
> - 一个上市公司退市的风险有多大？
> - 一个刚确诊的癌症患者还有多少时日？

这些看似无关的现象背后，其实都涉及一个共同的核心问题：事件是否发生及何时发生或事件发生的可能性有多大。在多数研究中，我们仅关心某个时点或某个状态，却很少关心「**持续时间**」，而这恰恰是「生存分析」的优势所在。

当我们讨论一个上市公司是否退市时，是不包含时间特征的。但若考虑它何时退市或退市的风险有多大时，就要增加对时间维度的考量。换言之，我们要探索的因变量为「事件是否发生」以及「一个状态转换到另外一个状态间的持续时间」，并要考察是哪些因素影响了事件的发生速度及生存时间的长短，则与之对应的分析方法为「**生存分析」** (Survival Analysis)。

目前，「**生存分析**」受到了经济学、金融学、社会学、管理学等领域的重视和青睐。为此，我们邀请了中央财经大学的王存同教授与大家分享生存分析的基本概念、主要模型和 Stata 实现方法。



## 1. 课程概览

- **简介：** 本课程介绍实证研究中常见的生存分析方法和模型，包括：描述性统计 (K-M 估计)、离散时间 Logit 模型、参数模型、分段恒定对数比率模型、比率风险模型等。
- **课程主页：** <https://gitee.com/arlionn/st>
- **授课方式：网络直播**，缴费后主办方发送邀请码点击邀请码即可进入在线课堂，收看直播无需安装任何软件。
- **时间：** 2020 年 6 月 6 日 (周六) 
  - **时段：** 上午 9:00-12:00; 下午 2:30-5:30；课后 30-60 分钟答疑
- **授课嘉宾：** [王存同](http://ssp.cufe.edu.cn/info/1127/3289.htm) 教授 (中央财经大学)
- **软件/课件：** Stata，提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)。建议使用 Stata 13.0 或更高版本，建议软件购买地址：[Stata 官网](http://www.stata.com/info/order/new/edu/gradplans/gp2-order.html) <http://www.stata.com>。
  - 课程大纲中涉及的参考文献下载链接：https://pan.baidu.com/s/149eF4l6GczGsaFiVII875g ；提取码：8mth 
- **学员优惠：** 一天回放 + 一个连享会[小直播课程](http://lianxh.duanshu.com)
- **报名链接：** <http://junquan18903405450.mikecrm.com/LyTjYYB> 

&emsp;

## 2. 嘉宾简介

[王存同](http://ssp.cufe.edu.cn/info/1127/3289.htm)，中央财经大学教授。博士毕业于北京大学 (与 University of Michigan 合作培养)，博士后研究员就职于美国伊利诺伊大学 (University of Illinois at Urbana-Champaign)。主要从事社会统计及计量经济分析、人口健康学等领域的研究与教学。2013 年入选教育部新世纪优秀人才计划，兼任北京大学社会科学方法培训教授、美国 PAA、国际 IUSSP 会员及 IUSSP 社会科学定量方法培训专家组成员、美国伊利诺伊大学合作研究员及中国青少年性健康教育委员会副主任委员等。曾在《中国社会科学》，《社会学研究》，_Chinese Sociological Review_, _Schizophrenia Research_ 等期刊发表论文近百篇，出版著作 6 部；主持国家社科基金项目 2 项、横向课题 12 项。

**小插曲：** 王存同教授长期兼任北京大学等多个高校社会科学方法的培训教授，授课生动活泼，通俗易懂，拥有众多粉丝。每逢王老师在中央财经大学沙河校区 (距离市区 1.5 小时车程) 开讲时，小伙伴们便像候鸟般赶赴沙河。每次上课总是座无虚席，或席地而坐、或背墙而立……若想在教室前排的过道台阶上抢个位置，那可是要早早出动的。

&emsp;

## 3. 课程详情

### 3.1 课程简介

生存分析在不同的学科里有不同的名称。例如，在经济学、社会学及管理学中也被称为「**事件史分析」** (Event History Analysis) 或「**久期分析**」(Duration Analysis)。

那么，用于生存分析的数据有什么特点呢？

以癌症患者为例，从确诊到死亡的时间是一个典型的连续数据。若研究者探索哪些因素 (如治疗方案、年龄、性别等影响该连续变量 (即生存时间)，根据「因变量的测量层次决定回归模型」的基本原则，似乎 OLS 就可以胜任，但事实上远没有那么简单。这是因为部分癌症患者的生存时间很短，部分患者生存时间很长 (甚至超过研究者的观察年限)，而我们限于观测时限、研究经费等因素，不可能永远跟踪每个患者并知道他/她确切的生存时间。因此，通常我们会有一个人为设定的观测区间 (如 4 年)。那么，在这个观测区间内，观测结果就会出现如下两种情况：

- 其一， 若该患者在 4 年的观测区间内去世，则可知道他/她确切的生存时间；
- 其二， 若该患者并未在观测区间内去世，则无法知道他/她确切的生存时间，但我们至少可以知道他/她的生存时间一定大于 4 年。这就出现了我们熟悉的数据缺失中数据被删截的现象，即出现了「右删截数据」；若这种数据缺失或删截与其它因素相关，则为数据被截除，即出现了「截除数据」。

数据被删截，以及时变变量 (time-dependent covariate) 的存在让我们很难再利用常规模型来处理事件史数据。例如，在癌症患者的案例中，我们感兴趣的事件是那些个体死亡的风险有多大，目的是研究有哪些因素影响了死亡的发生可能性以及什么时候发生。其中，部分自变量并不随时间发生变化，如性别、种族，而部分变量则随时间发生变化，如药物量、婚姻状况、是否喝酒与吸毒等。

若采用二分类因变量 Logit 模型 (死亡与否)，就会浪费关于何时死亡的时间信息。显然，在治疗之后一个月内就死亡的个体比那些在 48 个月内并没有死亡的个体，在死亡的可能性上是完全不同的。再如，我们可以建立一个代表患者从发病到死亡间所经历的时间的因变量，然后对这个因变量进行 OLS 分析。但那些在 48 个月之内都没有死亡的患者将会被「删截」，并将从分析中被剔除，从而导致估计偏倚。

由此可知，生存分析所对应的因变量是一个包含删截或截除数据的事件时间变量，即因变量同时包含了持续时间与事件发生的特征，为一个包含时间的复合变量。因此，对因变量考察时，类似对离散因变量的考察形式一样表现为概率模型，即包含「发生与否、何时发生」共性的概率。它包含了多种模型，如指数模型 (Exponential Model)、冈珀茨 (Gompertz) 模型、韦伯 (Weibull) 模型、离散时间模型 (Discrete-time Models) 以及考克斯 (Cox) 模型等。

「**生存分析**」在近期的 Top 期刊中得到了越来越广泛的关注和应用，涉及经济学、社会学、人口学、政治学、管理学、心理学、法律学等诸多学科。

本课程力图使学员在较短时间内了解常见生存分析模型的基本思想、原理、条件及适用范围，并以真实数据为演示案例，培训学员模型构建、软件应用及结果解读的能力，提高学员定量论文写作的水平。

在授课时间安排上，将利用少量时间进行回归知识的回顾，重点讲述生存分析的原理、模型设定、结果解读和 Stata 实操。例如，描述性统计 (K-M 估计)、参数模型、半参数模型、离散时间风险模型、分段指数模型等。

### 3.2 授课内容

本课程主要涵盖如下六个小专题。在讲解每个模型时，都将结合完整的 Stata 实现文档与学员共同进行实例操作，并提供完整的 **.dta**, **.do** 文档等资料，以便大家演练和应用。

各讲主要内容及参考文献列举如下：

**<font color=green>温馨提示</font>：** 下文列举的参考文献可以在线浏览 (手机浏览时需安装 PDF 浏览器)，亦可以通过百度云盘下载：
- 链接：https://pan.baidu.com/s/149eF4l6GczGsaFiVII875g
- 提取码：8mth 

&emsp;

> #### T1. 生存分析概述
- 生存分析的起源和基本概念
- 生存分析的应用场景
- 分析策略

> #### T2. 描述性分析：K-M 估计 (Kaplan–Meier estimator)

- Efron, B. (1988). Logistic regression, survival analysis, and the Kaplan-Meier curve. _Journal of the American statistical Association_, 83(402), 414-425. [[PDF]](https://quqi.gblhgk.com/s/880197/d5ATMBGv1OLHyOTQ)
- Peterson Jr, A. V. (1977). Expressing the Kaplan-Meier estimator as a function of empirical subsurvival functions. _Journal of the American Statistical Association_, 72(360a), 854-858. [[PDF]](https://quqi.gblhgk.com/s/880197/kdeccMFGeJ3q80nJ)

> #### T3. 离散时间 Logit 模型 (Discrete-Time Logit Models)

- Allison, P. D. (1982). Discrete-time methods for the analysis of event histories. _Sociological methodology_, 13, 61-98. [[PDF]](https://quqi.gblhgk.com/s/880197/he9w4VhljZl9yWhe)
- Yamaguchi, K. (1990). Logit and multinomial logit models for discrete-time event-history analysis: a causal analysis of interdependent discrete state processes. _Quality and Quantity_, 24(3), 323-341. [[PDF]](https://quqi.gblhgk.com/s/880197/O0shgMXb0YinSJYa)
- Muthén, B., & Masyn, K. (2005). Discrete-time survival mixture analysis. _Journal of Educational and Behavioral statistics_, 30(1), 27-58. [[PDF]](https://quqi.gblhgk.com/s/880197/Enr6j1fGLNQLYuoJ)

> #### T4. 参数模型 (Parametric Models)

主要介绍包含加速失效时间模型 (Accelerated Failure Time Models, AFT)。例如，对数正态模型 (Log-normal Models)、指数模型 (Exponential Models )、伽马模型 (Gamma Models)、Gompertz 模型及 Weibull 模型等。

- Lambert, P. C., & Royston, P. (2009). Further development of flexible parametric models for survival analysis. _The Stata Journal_, 9(2), 265-290. [[PDF]](https://quqi.gblhgk.com/s/880197/ORbnwnwkh3qpKfGL)
- Nelson, C. P., Lambert, P. C., Squire, I. B., & Jones, D. R. (2007). Flexible parametric models for relative survival, with application in coronary heart disease. _Statistics in Medicine_, 26(30), 5486-5498. [[PDF]](https://quqi.gblhgk.com/s/880197/YwHSN7cEg8qtDpXW)

> #### T5. 分段恒定对数比率模型 (Log-Rate Models for Piecewise Constant Rates)

- Lalive, R., Van Ours, J., & Zweimüller, J. (2006). How changes in financial incentives affect the duration of unemployment. _The Review of Economic Studies_, 73(4), 1009-1038. [[PDF]](https://quqi.gblhgk.com/s/880197/yFk0tOtKoeLwh5gP)
- Walder, A. G., Li, B., & Treiman, D. J. (2000). Politics and life chances in a state socialist regime: Dual career paths into the urban Chinese elite, 1949 to 1996. _American Sociological Review_, 191-209. [[PDF]](https://quqi.gblhgk.com/s/880197/8uVCllqi5Sf8KEBr)

> #### T6. 比例风险模型 (Proportional Hazards Modes)

主要包括 Cox 模型(Cox Model) 和分层 Cox 模型 (Stratified Cox Models)。

- Pourhoseingholi, M. A., Hajizadeh, E., Moghimi Dehkordi, B., Safaee, A., Abadi, A., & Zali, M. R. (2007). Comparing Cox regression and parametric models for survival of patients with gastric carcinoma. _Asian Pacific Journal of Cancer Prevention_, 8(3), 412. [[PDF]](https://quqi.gblhgk.com/s/880197/20LT2Uy9exLTJPCY)
- Tian, L., Zucker, D., & Wei, L. J. (2005). On the Cox model with time-varying regression coefficients. _Journal of the American statistical Association_, 100(469), 172-183. [[PDF]](https://quqi.gblhgk.com/s/880197/ttR6ywobj3UFyc3x)

&emsp;

### 3.3 课程特色

- **课程定位明确**。并非专业统计学课程，课堂上不会有烦琐的公式推导，重在应用，即在基本熟悉各模型思想的基础上，进行数据分析及模型解读，实现理论与应用相结合。
- **讲解重点突出**。讲解模型时，重点关注学术论文常用中高级模型，及其应用范围、条件、数据处理难点及模型参数的解读。
- **课件细致实用**。如各模型都附带完整的 Stata 实现过程（do 文档）和真实数据，学员无需进行烦琐编程，即可快速实现模型运用。
- **深度互动**。在课后答疑环节和课程微信群中，学员可就自己的研究数据进行沟通和解疑。
- **论文经验分享**。如与学员分享定量研究论文写作、投稿、修改，以及与编辑部、导师和合作者沟通的实用技巧。
- **讲师风格鲜明**。王老师风趣幽默，感染力强。在北京大学主讲全国高校教师社会科学定量方法暑期培训课时，多名学员评价其「统计课程如小说般引人入胜，不忍下课」、「讲解系统且实用性强」，已有多名学员应用所学成功发表量化研究论文。

<div STYLE="page-break-after: always;"></div>

### 3.4 课前准备

#### 预备知识

希望学员在开课前能重温计量经济学基础知识，对线性回归、假设检验有所了解，并具备一定的 Stata 软件操作基础。若对社会调查方法、社会统计有所了解，则更容易理解课程中的案例。当然，「大道至简、殊途同归」，

#### 参考教材
前3本为生存分析领域经典教科书，后2本偏向于Stata 应用。
- Allison, P. D., 1984. Event History Analysis (2nd) (No. 46). Sage. [[在线阅读]](http://methods.sagepub.com/Book/event-history-analysis/n1.xml)
- Yamaguchi, K., 1991. Event History Analysis. Sage. [[Link]](https://academic.microsoft.com/paper/2051285034/reference/search?q=Event%20History%20Analysis&qe=Or(Id%253D2796700885%252CId%253D2136883754%252CId%253D2004877180%252CId%253D2017871126%252CId%253D2090884151%252CId%253D2138646672%252CId%253D1979816698%252CId%253D2144240945%252CId%253D2313363431%252CId%253D1983779505%252CId%253D2007383086%252CId%253D2087543893%252CId%253D2024586593%252CId%253D2050060957%252CId%253D2036295941%252CId%253D2027727349%252CId%253D1922709871%252CId%253D2008233135%252CId%253D2053099995%252CId%253D2329718980%252CId%253D2051095011%252CId%253D2330672308%252CId%253D2331438887%252CId%253D2326648176%252CId%253D2329936004%252CId%253D2043843087%252CId%253D2332599966%252CId%253D1975869818%252CId%253D2312281180%252CId%253D2317409605%252CId%253D1966262225%252CId%253D2312409832%252CId%253D2270724572%252CId%253D1986537064)&f=&orderBy=0)，[[PDF]](https://www.jstage.jst.go.jp/article/ojjams/2/1/2_1_61/_pdf)
- Singer, J. D., Willett, J. B., & Willett, J. B., 2003. Applied Longitudinal Data Analysis: Modeling Change and Event Occurrence. Oxford university press. [[Link]](https://doi.org/10.1093/acprof:oso/9780195152968.001.0001)
- Cleves, Mario, William Gould, and Yulia Marchenko. An Introduction to Survival Analysis Using Stata, Revised Third Edition. 2016. Stata Press. [[Link]](https://www.stata.com/bookstore/survival-analysis-stata-introduction/)
- 陈强. 2014. 高级计量经济学及 Stata 应用 (第 2 版). 高等教育出版社. [[Link]](https://item.jd.com/12245387.html)

&emsp;



## 4. 报名和缴费信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**(含报名费、材料费)：880 元/人 (全价)
- **优惠方案**：
  - 三人及以上团购/连享会直播课老学员：9 折，792 元
  - 五人及以上团购/已充值连享会会员：8 折，704 元
  - 老学员优惠：8 折，704 元 (老学员: 此前参加过连享会现场班的学员)
  - **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 电话 (微信同号)： 王老师 18903405450 ; 李老师 ‭18636102467

&emsp;

### 报名链接

> **报名链接：** [http://junquan18903405450.mikecrm.com/LyTjYYB](http://junquan18903405450.mikecrm.com/LyTjYYB)                
> 或 长按/扫描二维码报名：   

![享会-生存分析专题-报名二维码](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码-st报名链接150.png)


&emsp;


### 缴费方式

> **方式1：对公转账**

- 户名：太原君泉教育咨询有限公司  
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式2：扫码支付**

<img style="width: 200px" src="https://images.gitee.com/uploads/images/2020/0510/143556_30fd6fdc_1522177.png">

> **温馨提示：** 扫码支付后，请将「**付款记录**」截屏发给王老师-18903405450（微信通号）



&emsp;

## 5. 诚聘助教

- [诚聘课程助教 (6名)](https://www.wjx.top/jq/75688406.aspx)，截止时间：2020 年 5 月 20 日
        

> **扫码报名：** https://www.wjx.top/jq/75688406.aspx  

![连享会-生存分析专题-助教招聘](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码-st助教招聘150.png)


<div STYLE="page-break-after: always;"></div>

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **连享会系列推文：** 将 [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 收藏起来，以便随时查看。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。

&emsp;

> #### 相关课程

- **TE 效率分析专题**，三天视频课程，嘉宾：连玉君，鲁晓东，张宁。[课程主页](https://gitee.com/arlionn/TE) (部分课件、板书和 FAQs 已经上传)，[PDF版](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)
- **文本分析与爬虫 - 专题视频**，随时报名随时学，主讲嘉宾：司继春，游万海，[课程主页](https://www.lianxh.cn/news/88426b2faeea8.html)    


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)


